package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    Scanner scanner = new Scanner(System.in);
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    public final static int PORT_NUMBER = 1234;
    private final String ip = "localhost";

    public static void main(String[] args) throws IOException {
        Client client = new Client();
        client.startConnection(PORT_NUMBER, client.ip);
    }

    public void startConnection(int portNumber, String ip) throws IOException {
        clientSocket = new Socket(ip, portNumber);
        System.out.println("You have accessed the server");
        communicate();
    }

    public void communicate() throws IOException {
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        String serverRespond;


        while ((serverRespond = in.readLine()) != null) {
            System.out.println(serverRespond);
            String output = scanner.nextLine();
            out.println(output);
        }
    }

}
