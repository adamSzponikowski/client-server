package server;

import client.Client;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;


public class Server {
    private String versionNumber = "0.1.0";
    private ServerSocket serverSocket;
    private Socket clientSocket;
    private ObjectMapper objectMapper = new ObjectMapper();
    private final Instant serverRun = Instant.now();
    private PrintWriter out;
    private BufferedReader in;
    private UserRepository userRepository = new UserRepository();
    private MessageRepository messageRepository = new MessageRepository();


    public String getVersionNumber(String versionNumber) {
        return versionNumber;
    }

    public static void main(String[] args) throws IOException, SQLException {
        Server server = new Server();
        server.start(Client.PORT_NUMBER);
    }

    public void start(int portNumber) throws IOException, SQLException {
        serverSocket = new ServerSocket(portNumber);
        clientSocket = serverSocket.accept();
        System.out.println("siema");
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        communicate();
    }


    public void communicate() throws IOException, SQLException {
        welcome();
        while (true) {
            String clientCommand = in.readLine();
            if (clientCommand.equals("1")) {
                login();
            }
            if (clientCommand.equals("2")) {
                creatingUser();
                communicate();
            }
        }
    }

    public void adminPanel(User user) throws IOException, SQLException {
        adminOptions();
        while (true) {
            String clientCommand = in.readLine();
            if (clientCommand.equals("uptime")) {
                serverLife();
            }
            if (clientCommand.equals("info")) {
                giveVersionNumber();
            }
            if (clientCommand.equals("help")) {
                adminOptions();
            }
            if (clientCommand.equals("stop")) {
                close();
            }
            if (clientCommand.equals("message")) {
                creatingMessage(user);
                adminPanel(user);
            }
            if (clientCommand.equals("log out")) {
                messageRepository.removeAMessage(user);
                communicate();
            }
        }
    }

    public void userPanel(User user) throws IOException, SQLException {
        userOptions();
        while (true) {
            String clientCommand = in.readLine();
            if (clientCommand.equals("message")) {
                creatingMessage(user);
                userPanel(user);
            }
            if (clientCommand.equals("log out")) {
                messageRepository.removeAMessage(user);
                communicate();
            }
        }
    }

    public void login() throws IOException, SQLException {
        out.println("Provide username:");
        String providedUsername = in.readLine();
        out.println("Provide password:");
        String providedPassword = in.readLine();
        User user = new User(providedUsername, providedPassword);
        if (userRepository.isUserAnAdmin(user)) {
            sendingMessageToClient(user);
            adminPanel(user);
        } else {
            sendingMessageToClient(user);
            userPanel(user);
        }
    }

    public void adminOptions() throws JsonProcessingException {
        ObjectNode adminOptions = objectMapper.createObjectNode();
        adminOptions.put("uptime", "Providing lifetime of the server");
        adminOptions.put("info", "Providing version number of the server");
        adminOptions.put("help", "Providing the list of commands");
        adminOptions.put("stop", "Stops the client and the server");
        adminOptions.put("message", "You can send a message");
        adminOptions.put("log out", "If you want to log out");
        respond(adminOptions);
    }

    public void userOptions() throws JsonProcessingException {
        ObjectNode userOptions = objectMapper.createObjectNode();
        userOptions.put("message", "If you want to send a message");
        userOptions.put("log out", "If you want to log out");
        respond(userOptions);
    }


    public void giveVersionNumber() throws JsonProcessingException {
        ObjectNode vN = objectMapper.createObjectNode();

        vN.put("Version", getVersionNumber(versionNumber));
        respond(vN);
    }

    public void serverLife() throws JsonProcessingException {
        ObjectNode objectServerLife = objectMapper.createObjectNode();

        Duration serverLife = Duration.between(serverRun, Instant.now());
        long hours = serverLife.toHours();
        long minutes = serverLife.toMinutes() % 60;
        long seconds = serverLife.toSeconds() % 60;
        String formattedDuration = String.format("%02d:%02d:%02d", hours, minutes, seconds);
        objectServerLife.put("Life duration", formattedDuration);
        respond(objectServerLife);
    }

    public void respond(ObjectNode objectNode) throws JsonProcessingException {
        String json = objectMapper.writeValueAsString(objectNode);
        out.println(json);
    }

    public void welcome() throws JsonProcessingException {
        ObjectNode welcome = objectMapper.createObjectNode();
        welcome.put("Welcome", "You have connected to the server");
        welcome.put("First option", "Press 1 if you have an account");
        welcome.put("Second option", "Press 2 if u want to create an account");
        respond(welcome);
    }

    public void close() throws IOException {
        out.close();
        in.close();
        clientSocket.close();
        serverSocket.close();
    }

    public void sendingMessageToClient(User user) throws SQLException, JsonProcessingException {
        List<String> messages = messageRepository.messages(user);
        ObjectNode response = objectMapper.createObjectNode();
        ArrayNode messagesNode = response.putArray("messages");
        if (messages.isEmpty()) {
            ObjectNode empty = objectMapper.createObjectNode();
            empty.put("message", "you have no messages");
            respond(empty);
        } else {
            for (String message : messages) {
                messagesNode.add(message);
            }
            respond(response);
        }
    }


    public void creatingUser() throws SQLException, IOException {
        ObjectNode provideUsername = objectMapper.createObjectNode();
        provideUsername.put("username", "provide username");
        respond(provideUsername);
        String username = in.readLine();
        ObjectNode providePassword = objectMapper.createObjectNode();
        providePassword.put("password", "provide password");
        respond(providePassword);
        String password = in.readLine();
        ObjectNode createdAnAccount = objectMapper.createObjectNode();
        User newUser = new User(username, password);
        userRepository.saveUser(newUser);
        createdAnAccount.put("Server message:", "You have created an account, you can now log in");
        respond(createdAnAccount);
    }

    public void creatingMessage(User user) throws IOException, SQLException {
        ObjectNode receiverUsername = objectMapper.createObjectNode();
        receiverUsername.put("receiver username", "Provide the username of the user you want to send a message to");
        respond(receiverUsername);
        String receiver = in.readLine();
        ObjectNode messageContent = objectMapper.createObjectNode();
        messageContent.put("message content", "provide the message content");
        respond(messageContent);
        String content = in.readLine();
        ObjectNode createdAnMessage = objectMapper.createObjectNode();
        createdAnMessage.put("Server message", "You have send an message");
        respond(createdAnMessage);
        String sender = user.getUsername();
        Message newMessage = new Message(receiver, content, sender);
        messageRepository.creatingNewMessage(newMessage);
    }
}
