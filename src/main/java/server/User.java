package server;


import java.util.List;
import java.util.Objects;

public class User {
    private String password;
    private String username;
    public List<Message> messageList;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User() {
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(username) + Objects.hashCode(password);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        User user = (User) obj;

       return user.getUsername().equals(username) && user.getPassword().equals(password);
    }
}
