package server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserRepository {

    private DatabaseMenager databaseMenager = new DatabaseMenager();

    public void saveUser(User newUser) throws SQLException {
        Connection connection = databaseMenager.getConnection();
        String credientials = "INSERT INTO users (username, password) VALUES (?, ?)";
        PreparedStatement statement = connection.prepareStatement(credientials);
        statement.setString(1, newUser.getUsername());
        statement.setString(2, newUser.getPassword());
        statement.executeUpdate();
    }

    public boolean userValidation(User user) throws SQLException {
        List<User> users = new ArrayList<>();
        Connection connection = databaseMenager.getConnection();
        String selectQuery = "SELECT username,password FROM users";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        ResultSet resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            String username = resultSet.getString("username");
            String password = resultSet.getString("password");
            User user1 = new User(username, password);
            users.add(user1);
        }
        return users.contains(user);
    }

    public boolean isUserAnAdmin(User user) throws SQLException {
        Connection connection = databaseMenager.getConnection();
        String selectQuery = "SELECT username,password FROM admin";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            String username = resultSet.getString("username");
            String password = resultSet.getString("password");
            User admin = new User(username, password);
            return admin.equals(user);
        }
        return false;
    }
}
