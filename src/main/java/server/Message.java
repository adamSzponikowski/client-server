package server;
public class Message {
    private String sender;
    private String receiver;
    private String content;

    public Message(String receiver, String content,String sender) {
        this.sender = sender;
        this.receiver = receiver;
        this.content = content;
    }

    public Message() {

    }
    public String getSender(){
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getContent() {
        return content;
    }
}
