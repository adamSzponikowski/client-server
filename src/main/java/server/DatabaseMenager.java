package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseMenager {
    String url = "jdbc:sqlite:/Users/adamszponikowski/Desktop/database/clientserver.db";

    public Connection getConnection() {
        try {
            Connection conn = DriverManager.getConnection(url);
            return conn;
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }
}