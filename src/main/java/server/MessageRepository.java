package server;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MessageRepository {
    DatabaseMenager databaseMenager = new DatabaseMenager();

    public void creatingNewMessage(Message newMessage) throws SQLException {
        Connection connection = databaseMenager.getConnection();
        String message = "INSERT INTO messages (sender, receiver, content) VALUES (?, ?, ?)";
        PreparedStatement statement = connection.prepareStatement(message);
        statement.setString(1, newMessage.getSender());
        statement.setString(2, newMessage.getReceiver());
        statement.setString(3, newMessage.getContent());
        statement.executeUpdate();
    }

    public void removeAMessage(User user) throws SQLException {
        Connection connection = databaseMenager.getConnection();
        String deleteQuery = "DELETE from messages where receiver = ?";
        PreparedStatement statement = connection.prepareStatement(deleteQuery);
        statement.setString(1, user.getUsername());
        statement.executeUpdate();
    }

    public List<String> messages(User user) throws SQLException {
        Connection connection = databaseMenager.getConnection();
        String selectQuery = "SELECT content from messages where receiver = ? ";
        PreparedStatement statement = connection.prepareStatement(selectQuery);
        statement.setString(1, user.getUsername());
        ResultSet resultSet = statement.executeQuery();
        List<String> messages = new ArrayList<>();
        while (resultSet.next()) {
            String content = resultSet.getString("content");
            messages.add(content);
        }
        return messages;
    }
}
