package server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MessageRepositoryTest {
    private MessageRepository messageRepository = new MessageRepository();
    private DatabaseMenager databaseMenager = new DatabaseMenager();

    @Test
    public void isMessageBeingCreated() throws SQLException {
        //Given
        Message newMessage = new Message("adam", "siema", "John Rambo");
        //When
        messageRepository.creatingNewMessage(newMessage);
        //Then
        Connection connection = databaseMenager.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String selectQuery = "SELECT receiver FROM messages WHERE receiver = ?";
        preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, "adam");

        resultSet = preparedStatement.executeQuery();
        assertTrue(resultSet.next(), "Message should be in database");

        String deleteQuery = "DELETE FROM messages WHERE receiver = ?";
        preparedStatement = connection.prepareStatement(deleteQuery);
        preparedStatement.setString(1, "adam");
        preparedStatement.executeUpdate();
    }

    @Test
    public void isMessageBeingRemoved() throws SQLException {
        //Given
        User newUser = new User("adam", "adam");
        Message newMessage = new Message("adam", "siema", "John Rambo");
        //When
        messageRepository.creatingNewMessage(newMessage);
        messageRepository.removeAMessage(newUser);
        //Then

        Connection connection = databaseMenager.getConnection();
        String query = "SELECT receiver from messages where receiver = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1, "adam");

        ResultSet resultSet = null;
        resultSet = preparedStatement.executeQuery();
        assertFalse(resultSet.next(), "Message should not be save in database");

        String deleteQuery = "DELETE FROM users WHERE username = ?";
        preparedStatement = connection.prepareStatement(deleteQuery);
        preparedStatement.setString(1, "adam");
        preparedStatement.executeUpdate();
    }

    @Test
    public void isTheEmptyListOfMessegesBeingReturn() throws SQLException {
        //Given
        User newUser = new User("adam", "adam");
        //When
        List<String> messages = messageRepository.messages(newUser);
        //Then
        Assertions.assertTrue(messages.isEmpty());


        Connection connection = databaseMenager.getConnection();
        String deleteQuery = "DELETE FROM users WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery);
        preparedStatement.setString(1, "adam");
        preparedStatement.executeUpdate();
    }

    @Test
    public void isMessageBeingReturned() throws SQLException {
        //Given
        User newUser = new User("adam", "adam");
        Message newMessage = new Message("adam", "siema", "John Rambo");
        //When
        messageRepository.creatingNewMessage(newMessage);
        List<String> messages = messageRepository.messages(newUser);
        //Then
        Assertions.assertFalse(messages.isEmpty());
        Connection connection = databaseMenager.getConnection();
        String deleteQuery = "DELETE FROM users WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery);
        preparedStatement.setString(1, "adam");
        preparedStatement.executeUpdate();
        String deleteMessage = "DELETE FROM messages WHERE receiver = ?";
        PreparedStatement statement = connection.prepareStatement(deleteMessage);
        statement.setString(1, "adam");
        statement.executeUpdate();
    }

}