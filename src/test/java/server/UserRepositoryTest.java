package server;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UserRepositoryTest {

    private UserRepository userRepository = new UserRepository();
    private DatabaseMenager databaseMenager = new DatabaseMenager();

    @Test
    public void isTheUserBeingSavedInDataBase() throws SQLException {
        //given
        User newUser = new User("adam", "adam");
        //when
        userRepository.saveUser(newUser);
        //then
        Connection connection = databaseMenager.getConnection();
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        String selectQuery = "SELECT username FROM users WHERE username = ?";
        preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, newUser.getUsername());

        resultSet = preparedStatement.executeQuery();
        assertTrue(resultSet.next(), "User should be save in database");

        // Clean up: Delete the user
        String deleteQuery = "DELETE FROM users WHERE username = ?";
        preparedStatement = connection.prepareStatement(deleteQuery);
        preparedStatement.setString(1, newUser.getUsername());
        preparedStatement.executeUpdate();
    }

    @Test
    public void isTheUserValidationCorrect() throws SQLException {
        //given
        User newUser = new User("adam", "adam");
        User falseUser = new User("JohnRambo", "JohnRambo");
        Connection connection = databaseMenager.getConnection();
        //when
        userRepository.saveUser(newUser);
        boolean validation = userRepository.userValidation(newUser);
        boolean falseValidation = userRepository.userValidation(falseUser);
        //then
        Assertions.assertTrue(validation);
        Assertions.assertFalse(falseValidation);
        // Clean up: Delete the user
        String deleteQuery = "DELETE FROM users WHERE username = ?";
        PreparedStatement preparedStatement = connection.prepareStatement(deleteQuery);
        preparedStatement.setString(1, newUser.getUsername());
        preparedStatement.executeUpdate();
    }

    @Test
    public void isUserAnAdmin() throws SQLException {
        //given
        User admin = new User("admin", "admin");
        User falseUser = new User("JohnRambo", "JohnRambo");
        //when
        boolean validation = userRepository.isUserAnAdmin(admin);
        boolean falseValidation = userRepository.isUserAnAdmin(falseUser);
        //then
        Assertions.assertTrue(validation);
        Assertions.assertFalse(falseValidation);
    }
}
